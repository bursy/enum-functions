public class SomeService {
    public static void someUsage() {
        Pair pair = new Pair("isNotEmpty", "abc");

        MyPredicate predicate = PredicateType.byValue(pair.left).createPredicate(pair.right);
    }
}
