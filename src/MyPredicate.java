public class MyPredicate {
    private static final MyPredicate FOO = new MyPredicate();

    public static MyPredicate isNotEmpty(String s) {
        return FOO;
    }

    public static MyPredicate isEmpty(String s) {
        return FOO;
    }
}
