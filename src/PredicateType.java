import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum PredicateType {
    IS_NOT_EMPTY("isNotEmpty", s -> MyPredicate.isNotEmpty(s)),
    IS_EMPTY("isEmpty", s -> MyPredicate.isEmpty(s));

    private final String value;
    private final Function<String, MyPredicate> function;

    PredicateType(String value, Function<String, MyPredicate> function) {
        this.value = value;
        this.function = function;
    }

    public MyPredicate createPredicate(String field) {
        return function.apply(field);
    }

    public static PredicateType byValue(String value) {
        // XXX too lazy to do right
        return Arrays.stream(values()).filter(type -> type.value.equals(value)).collect(Collectors.toList()).get(0);
    }
}
